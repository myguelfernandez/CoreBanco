package py.sd.corebanco.mappers;

import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import py.sd.corebanco.pojos.Cuentas;
import py.sd.corebanco.pojos.CuentasExample;

public interface CuentasMapper {
    @SelectProvider(type=CuentasSqlProvider.class, method="countByExample")
    int countByExample(CuentasExample example);

    @DeleteProvider(type=CuentasSqlProvider.class, method="deleteByExample")
    int deleteByExample(CuentasExample example);

    @Delete({
        "delete from cuentas",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into cuentas (numero_cuenta, nombre_cuenta, ",
        "estado, saldo)",
        "values (#{numeroCuenta,jdbcType=INTEGER}, #{nombreCuenta,jdbcType=VARCHAR}, ",
        "#{estado,jdbcType=BIT}, #{saldo,jdbcType=DOUBLE})"
    })
    @Options(useGeneratedKeys=true,keyProperty="id")
    int insert(Cuentas record);

    @InsertProvider(type=CuentasSqlProvider.class, method="insertSelective")
    @Options(useGeneratedKeys=true,keyProperty="id")
    int insertSelective(Cuentas record);

    @SelectProvider(type=CuentasSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="numero_cuenta", property="numeroCuenta", jdbcType=JdbcType.INTEGER),
        @Result(column="nombre_cuenta", property="nombreCuenta", jdbcType=JdbcType.VARCHAR),
        @Result(column="estado", property="estado", jdbcType=JdbcType.BIT),
        @Result(column="saldo", property="saldo", jdbcType=JdbcType.DOUBLE)
    })
    List<Cuentas> selectByExample(CuentasExample example);

    @Select({
        "select",
        "id, numero_cuenta, nombre_cuenta, estado, saldo",
        "from cuentas",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="numero_cuenta", property="numeroCuenta", jdbcType=JdbcType.INTEGER),
        @Result(column="nombre_cuenta", property="nombreCuenta", jdbcType=JdbcType.VARCHAR),
        @Result(column="estado", property="estado", jdbcType=JdbcType.BIT),
        @Result(column="saldo", property="saldo", jdbcType=JdbcType.DOUBLE)
    })
    Cuentas selectByPrimaryKey(Integer id);

    @UpdateProvider(type=CuentasSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") Cuentas record, @Param("example") CuentasExample example);

    @UpdateProvider(type=CuentasSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") Cuentas record, @Param("example") CuentasExample example);

    @UpdateProvider(type=CuentasSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(Cuentas record);

    @Update({
        "update cuentas",
        "set numero_cuenta = #{numeroCuenta,jdbcType=INTEGER},",
          "nombre_cuenta = #{nombreCuenta,jdbcType=VARCHAR},",
          "estado = #{estado,jdbcType=BIT},",
          "saldo = #{saldo,jdbcType=DOUBLE}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(Cuentas record);
}