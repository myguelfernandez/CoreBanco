package py.sd.corebanco.mappers;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.DELETE_FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.INSERT_INTO;
import static org.apache.ibatis.jdbc.SqlBuilder.ORDER_BY;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT_DISTINCT;
import static org.apache.ibatis.jdbc.SqlBuilder.SET;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.UPDATE;
import static org.apache.ibatis.jdbc.SqlBuilder.VALUES;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;

import java.util.List;
import java.util.Map;
import py.sd.corebanco.pojos.Cuentas;
import py.sd.corebanco.pojos.CuentasExample.Criteria;
import py.sd.corebanco.pojos.CuentasExample.Criterion;
import py.sd.corebanco.pojos.CuentasExample;

public class CuentasSqlProvider {

    public String countByExample(CuentasExample example) {
        BEGIN();
        SELECT("count(*)");
        FROM("cuentas");
        applyWhere(example, false);
        return SQL();
    }

    public String deleteByExample(CuentasExample example) {
        BEGIN();
        DELETE_FROM("cuentas");
        applyWhere(example, false);
        return SQL();
    }

    public String insertSelective(Cuentas record) {
        BEGIN();
        INSERT_INTO("cuentas");
        
        if (record.getNumeroCuenta() != null) {
            VALUES("numero_cuenta", "#{numeroCuenta,jdbcType=INTEGER}");
        }
        
        if (record.getNombreCuenta() != null) {
            VALUES("nombre_cuenta", "#{nombreCuenta,jdbcType=VARCHAR}");
        }
        
        if (record.getEstado() != null) {
            VALUES("estado", "#{estado,jdbcType=BIT}");
        }
        
        if (record.getSaldo() != null) {
            VALUES("saldo", "#{saldo,jdbcType=DOUBLE}");
        }
        
        return SQL();
    }

    public String selectByExample(CuentasExample example) {
        BEGIN();
        if (example != null && example.isDistinct()) {
            SELECT_DISTINCT("id");
        } else {
            SELECT("id");
        }
        SELECT("numero_cuenta");
        SELECT("nombre_cuenta");
        SELECT("estado");
        SELECT("saldo");
        FROM("cuentas");
        applyWhere(example, false);
        
        if (example != null && example.getOrderByClause() != null) {
            ORDER_BY(example.getOrderByClause());
        }
        
        return SQL();
    }

    public String updateByExampleSelective(Map<String, Object> parameter) {
        Cuentas record = (Cuentas) parameter.get("record");
        CuentasExample example = (CuentasExample) parameter.get("example");
        
        BEGIN();
        UPDATE("cuentas");
        
        if (record.getId() != null) {
            SET("id = #{record.id,jdbcType=INTEGER}");
        }
        
        if (record.getNumeroCuenta() != null) {
            SET("numero_cuenta = #{record.numeroCuenta,jdbcType=INTEGER}");
        }
        
        if (record.getNombreCuenta() != null) {
            SET("nombre_cuenta = #{record.nombreCuenta,jdbcType=VARCHAR}");
        }
        
        if (record.getEstado() != null) {
            SET("estado = #{record.estado,jdbcType=BIT}");
        }
        
        if (record.getSaldo() != null) {
            SET("saldo = #{record.saldo,jdbcType=DOUBLE}");
        }
        
        applyWhere(example, true);
        return SQL();
    }

    public String updateByExample(Map<String, Object> parameter) {
        BEGIN();
        UPDATE("cuentas");
        
        SET("id = #{record.id,jdbcType=INTEGER}");
        SET("numero_cuenta = #{record.numeroCuenta,jdbcType=INTEGER}");
        SET("nombre_cuenta = #{record.nombreCuenta,jdbcType=VARCHAR}");
        SET("estado = #{record.estado,jdbcType=BIT}");
        SET("saldo = #{record.saldo,jdbcType=DOUBLE}");
        
        CuentasExample example = (CuentasExample) parameter.get("example");
        applyWhere(example, true);
        return SQL();
    }

    public String updateByPrimaryKeySelective(Cuentas record) {
        BEGIN();
        UPDATE("cuentas");
        
        if (record.getNumeroCuenta() != null) {
            SET("numero_cuenta = #{numeroCuenta,jdbcType=INTEGER}");
        }
        
        if (record.getNombreCuenta() != null) {
            SET("nombre_cuenta = #{nombreCuenta,jdbcType=VARCHAR}");
        }
        
        if (record.getEstado() != null) {
            SET("estado = #{estado,jdbcType=BIT}");
        }
        
        if (record.getSaldo() != null) {
            SET("saldo = #{saldo,jdbcType=DOUBLE}");
        }
        
        WHERE("id = #{id,jdbcType=INTEGER}");
        
        return SQL();
    }

    protected void applyWhere(CuentasExample example, boolean includeExamplePhrase) {
        if (example == null) {
            return;
        }
        
        String parmPhrase1;
        String parmPhrase1_th;
        String parmPhrase2;
        String parmPhrase2_th;
        String parmPhrase3;
        String parmPhrase3_th;
        if (includeExamplePhrase) {
            parmPhrase1 = "%s #{example.oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{example.oredCriteria[%d].allCriteria[%d].value} and #{example.oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{example.oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{example.oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{example.oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        } else {
            parmPhrase1 = "%s #{oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{oredCriteria[%d].allCriteria[%d].value} and #{oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        }
        
        StringBuilder sb = new StringBuilder();
        List<Criteria> oredCriteria = example.getOredCriteria();
        boolean firstCriteria = true;
        for (int i = 0; i < oredCriteria.size(); i++) {
            Criteria criteria = oredCriteria.get(i);
            if (criteria.isValid()) {
                if (firstCriteria) {
                    firstCriteria = false;
                } else {
                    sb.append(" or ");
                }
                
                sb.append('(');
                List<Criterion> criterions = criteria.getAllCriteria();
                boolean firstCriterion = true;
                for (int j = 0; j < criterions.size(); j++) {
                    Criterion criterion = criterions.get(j);
                    if (firstCriterion) {
                        firstCriterion = false;
                    } else {
                        sb.append(" and ");
                    }
                    
                    if (criterion.isNoValue()) {
                        sb.append(criterion.getCondition());
                    } else if (criterion.isSingleValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase1, criterion.getCondition(), i, j));
                        } else {
                            sb.append(String.format(parmPhrase1_th, criterion.getCondition(), i, j,criterion.getTypeHandler()));
                        }
                    } else if (criterion.isBetweenValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase2, criterion.getCondition(), i, j, i, j));
                        } else {
                            sb.append(String.format(parmPhrase2_th, criterion.getCondition(), i, j, criterion.getTypeHandler(), i, j, criterion.getTypeHandler()));
                        }
                    } else if (criterion.isListValue()) {
                        sb.append(criterion.getCondition());
                        sb.append(" (");
                        List<?> listItems = (List<?>) criterion.getValue();
                        boolean comma = false;
                        for (int k = 0; k < listItems.size(); k++) {
                            if (comma) {
                                sb.append(", ");
                            } else {
                                comma = true;
                            }
                            if (criterion.getTypeHandler() == null) {
                                sb.append(String.format(parmPhrase3, i, j, k));
                            } else {
                                sb.append(String.format(parmPhrase3_th, i, j, k, criterion.getTypeHandler()));
                            }
                        }
                        sb.append(')');
                    }
                }
                sb.append(')');
            }
        }
        
        if (sb.length() > 0) {
            WHERE(sb.toString());
        }
    }
}