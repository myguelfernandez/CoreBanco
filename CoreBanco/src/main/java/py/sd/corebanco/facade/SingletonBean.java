/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.sd.corebanco.facade;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionManagement;
import static javax.ejb.TransactionManagementType.BEAN;
import javax.transaction.UserTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.ConfigurationSource;
import org.apache.logging.log4j.core.config.Configurator;
import py.sd.corebanco.Constantes;
import py.sd.corebanco.pojos.ConfiguracionesExample;
import py.sd.corebanco.pojos.CuentasExample;
import py.sd.corebanco.pojos.TokensExample;
import py.sd.corebanco.pojos.Usuarios;
import py.sd.corebanco.pojos.UsuariosExample;


/**
 *
 * @author Miguel Fernandez <myguelfernandez@gmail.com>
 */
@Singleton
@Startup
@TransactionManagement(BEAN)
public class SingletonBean {

    private final Logger logger = LogManager.getLogger(this.getClass());

    @EJB
    ConfiguracionesFacade configuracionesFacade;
    @EJB
    UsuariosFacade usuariosFacade;
    @EJB
    TokensFacade tokensFacade;
    @EJB
    CuentasFacade cuentasFacade;

    @Resource
    UserTransaction userTransaction;

    @PostConstruct
    public void init() {

        try {
            System.out.println("init");
            InputStream log4j2 = this.getClass().getClassLoader().getResourceAsStream("log4j2.xml");
            System.out.println("log " + log4j2);
            ConfigurationSource source = new ConfigurationSource(log4j2);
            Configurator.initialize(null, source);
            
            
            Properties properties = new Properties();
            FileInputStream fileInput;
            fileInput = new FileInputStream(Constantes.FILE_CONFIG_PATH);
            properties.load(fileInput);
            Constantes.URL_WS = properties.getProperty("url.casacambio");
            Constantes.USER = properties.getProperty("user");
            Constantes.PASSWORD = properties.getProperty("pass");
        } catch (Exception e) {
            System.out.println("ERROR");
            e.printStackTrace();
        }

        try {

            logger.info("==========================");
            userTransaction.begin();
            logger.info("Test de Conexiones");
            ConfiguracionesExample example = new ConfiguracionesExample();
            int i = configuracionesFacade.countByExample(example);
            logger.info("cantidad configuraciones: " + i);
            UsuariosExample usurarioExample = new UsuariosExample();
            int j = usuariosFacade.countByExample(usurarioExample);
            logger.info("cantidad usuarios: " + j);
            TokensExample tokensExample = new TokensExample();
            int k = tokensFacade.countByExample(tokensExample);
            logger.info("cantidad tokens: " + k);
            CuentasExample cuentaExample = new CuentasExample();
            int l = cuentasFacade.countByExample(cuentaExample);
            logger.info("cantidad de cuentas: " + l);
            Usuarios nuevoUsuario = new Usuarios();
            nuevoUsuario.setUsuario("miguel");
            nuevoUsuario.setPassword("123");

            userTransaction.commit();
            logger.info("==========================");
        } catch (Exception e) {
            try {
            } catch (Exception ex) {
                logger.error(ex.getMessage(),ex);
            }
            logger.error(e.getMessage(), e);
        }

    }
}
