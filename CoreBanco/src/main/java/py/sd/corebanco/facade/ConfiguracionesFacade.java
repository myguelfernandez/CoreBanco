/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.sd.corebanco.facade;

import javax.ejb.Stateless;
import javax.inject.Inject;
import org.mybatis.cdi.Mapper;
import py.sd.corebanco.mappers.ConfiguracionesMapper;
import py.sd.corebanco.pojos.Configuraciones;
import py.sd.corebanco.pojos.ConfiguracionesExample;


/**
 *
 * @author Miguel Fernandez <myguelfernandez@gmail.com>
 */
@Stateless
public class ConfiguracionesFacade extends GenericFacade<Configuraciones, Long, ConfiguracionesExample, ConfiguracionesMapper>{


}
