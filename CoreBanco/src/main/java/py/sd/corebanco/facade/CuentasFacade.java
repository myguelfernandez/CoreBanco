/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.sd.corebanco.facade;

import javax.ejb.Stateless;
import javax.inject.Inject;
import org.mybatis.cdi.Mapper;
import py.sd.corebanco.mappers.CuentasMapper;
import py.sd.corebanco.pojos.Cuentas;
import py.sd.corebanco.pojos.CuentasExample;


/**
 *
 * @author Miguel Fernandez <miguel.fernandez@konecta.com.py>
 */
@Stateless
public class CuentasFacade extends GenericFacade<Cuentas, Long, CuentasExample, CuentasMapper>{


    
}
