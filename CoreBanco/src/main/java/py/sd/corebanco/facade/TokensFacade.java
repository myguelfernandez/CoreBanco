/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.sd.corebanco.facade;

import javax.ejb.Stateless;
import javax.inject.Inject;
import org.mybatis.cdi.Mapper;
import py.sd.corebanco.mappers.TokensMapper;
import py.sd.corebanco.pojos.Tokens;
import py.sd.corebanco.pojos.TokensExample;


/**
 *
 * @author Miguel Fernandez <myguelfernandez@gmail.com>
 */
@Stateless
public class TokensFacade extends GenericFacade<Tokens, Long, TokensExample, TokensMapper>{

    
}
