/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.sd.corebanco;

import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import py.sd.casacambio.CambioRespuesta;

/**
 *
 * @author Miguel Fernandez <myguelfernandez@gmail.com>
 */
public class CasaCambioConsume {

    private static final Logger logger = LogManager.getLogger(CasaCambioConsume.class);

    public static py.sd.casacambio.LoginRespuesta login(String user, String pass) {
        py.sd.casacambio.LoginRespuesta resp = new py.sd.casacambio.LoginRespuesta();
        Gson gson = new Gson();
        logger.info("IN: [{}]", user, pass);

        try {
            py.sd.casacambio.CasaCambioWs_Service service = new py.sd.casacambio.CasaCambioWs_Service();
            py.sd.casacambio.CasaCambioWs port = service.getCasaCambioWsPort();

            resp = port.login(user, pass);
            System.out.println("Result = " + resp);
        } catch (Exception ex) {
            resp.setEstado(-1);
            resp.setMensaje("servidor de Casa Cambio no disponible");
            logger.error(ex.getMessage(), ex);
        }

        logger.info("OUT: " + gson.toJson(resp));
        return resp;
    }

    public static py.sd.casacambio.CambioRespuesta compra(Integer moneda, Double monto, String token) {
        logger.info("IN: [{}]", moneda, monto, token);
        py.sd.casacambio.CambioRespuesta result = new CambioRespuesta();
        Gson gson = new Gson();
        
        try { // Call Web Service Operation
            py.sd.casacambio.CasaCambioWs_Service service = new py.sd.casacambio.CasaCambioWs_Service();
            py.sd.casacambio.CasaCambioWs port = service.getCasaCambioWsPort();
            // TODO initialize WS operation arguments here

            // TODO process result here
            result = port.compra(moneda, monto, token);
        } catch (Exception ex) {
            logger.error(ex.getMessage(),ex);
            result.setEstado(-1);
            result.setMensaje("Error indeterminado");
        }

        logger.info("OUT: " + result);
        logger.info("OUT: " + gson.toJson(result));
        return result;

    }

    public static py.sd.casacambio.CambioRespuesta venta(Integer moneda, Double monto, String token) {
        logger.info("IN: [{}]", moneda, monto, token);
        py.sd.casacambio.CambioRespuesta result = new CambioRespuesta();
        Gson gson = new Gson();
        
        try { // Call Web Service Operation
            py.sd.casacambio.CasaCambioWs_Service service = new py.sd.casacambio.CasaCambioWs_Service();
            py.sd.casacambio.CasaCambioWs port = service.getCasaCambioWsPort();
            // TODO initialize WS operation arguments here

            // TODO process result here
            result = port.venta(moneda, monto, token);
        } catch (Exception ex) {
            logger.error(ex.getMessage(),ex);
            result.setEstado(-1);
            result.setMensaje("Error indeterminado");
        }

        logger.info("OUT: " + result);
        logger.info("OUT: " + gson.toJson(result));
        return result;

    }
}
