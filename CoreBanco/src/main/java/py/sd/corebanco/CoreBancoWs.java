/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.sd.corebanco;

import com.google.gson.Gson;
import java.util.List;
import java.util.Random;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.naming.InitialContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import py.sd.corebanco.facade.CuentasFacade;
import py.sd.corebanco.facade.TokensFacade;
import py.sd.corebanco.facade.UsuariosFacade;
import py.sd.corebanco.pojos.Cuentas;
import py.sd.corebanco.pojos.CuentasExample;
import py.sd.corebanco.pojos.Tokens;
import py.sd.corebanco.pojos.TokensExample;
import py.sd.corebanco.pojos.UsuariosExample;

/**
 *
 * @author Miguel Fernandez <myguelfernandez@gmail.com>
 */
@WebService(serviceName = "CoreBancoWs")
public class CoreBancoWs {

    private final Logger logger = LogManager.getLogger(this.getClass());

    InitialContext ctx;
    UsuariosFacade usuariosFacade;
    TokensFacade tokensFacade;
    CuentasFacade cuentasFacade;

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "login")
    public LoginRespuesta login(@WebParam(name = "user") String user, @WebParam(name = "pass") String pass) {
        LoginRespuesta resp = new LoginRespuesta();
        logger.info("user: " + user + " pass: " + pass);
        try {

            ctx = new InitialContext();
            usuariosFacade = (UsuariosFacade) ctx.lookup("java:global/CoreBanco/UsuariosFacade");
            tokensFacade = (TokensFacade) ctx.lookup("java:global/CoreBanco/TokensFacade");

            //Verificar que exita el usuario
            UsuariosExample example = new UsuariosExample();
            example.createCriteria()
                    .andUsuarioEqualTo(user)
                    .andPasswordEqualTo(pass)
                    .andEstadoEqualTo(Boolean.TRUE);

            int count = usuariosFacade.countByExample(example);

            if (count > 0) {
                Random rand = new Random(System.currentTimeMillis());
                //generar token
                Integer token = rand.nextInt(99999999) + 100000;
                Tokens tokenRecord = new Tokens();
                tokenRecord.setEstado(Boolean.TRUE);
                tokenRecord.setToken(String.valueOf(token));
                tokenRecord.setUsuario(user);
                int tokenInseratdo = tokensFacade.insert(tokenRecord);
                if (tokenInseratdo > 0) {
                    resp.setEstado(0);
                    resp.setMensaje("OK");
                    resp.setToken(String.valueOf(token));
                } else {
                    resp.setEstado(-1);
                    resp.setMensaje("error al generar y guardar el token, Intentelo de nuevo");
                }
            } else {
                resp.setEstado(-1);
                resp.setMensaje("usuario no existe o no esta activo");
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            resp.setEstado(-1);
            resp.setMensaje(e.getMessage());
        }
        logger.info("OUT: " + resp);
        return resp;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "deposito")
    public BaseRespuesta deposito(@WebParam(name = "cuenta") Integer cuenta, @WebParam(name = "monto") Double monto, @WebParam(name = "token") String token) {
        BaseRespuesta resp = new BaseRespuesta();
        try {
            ctx = new InitialContext();
            tokensFacade = (TokensFacade) ctx.lookup("java:global/CoreBanco/TokensFacade");
            cuentasFacade = (CuentasFacade) ctx.lookup("java:global/CoreBanco/CuentasFacade");

            //verificamos token
            TokensExample tokensExample = new TokensExample();
            tokensExample.createCriteria()
                    .andTokenEqualTo(token)
                    .andEstadoEqualTo(Boolean.TRUE);
            int tokenCount = tokensFacade.countByExample(tokensExample);

            if (tokenCount > 0) {
                //verficamos cuenta origen
                CuentasExample cuentasOrigenExample = new CuentasExample();
                cuentasOrigenExample.createCriteria()
                        .andNumeroCuentaEqualTo(1)
                        .andEstadoEqualTo(Boolean.TRUE);
                List<Cuentas> cuentas = cuentasFacade.selectByExample(cuentasOrigenExample);
                if (cuentas.size() > 0) {
                    Cuentas cuentaOrigen = cuentas.get(0);
                    //verificamos que tenga saldo suficiente para la transferencia
                    if (cuentaOrigen.getSaldo() >= monto) {
                        //verificamos cuenta destino
                        CuentasExample cuentasDestinoExample = new CuentasExample();
                        cuentasDestinoExample.createCriteria()
                                .andNumeroCuentaEqualTo(cuenta)
                                .andEstadoEqualTo(Boolean.TRUE);
                        List<Cuentas> cuentasDestinos = cuentasFacade.selectByExample(cuentasDestinoExample);
                        if (cuentasDestinos.size() > 0) {
                            Cuentas destinoCuenta = cuentasDestinos.get(0);
                            Double nuevoMontoOrigen = cuentaOrigen.getSaldo() - monto;
                            Double nuevoMontoDestino = destinoCuenta.getSaldo() + monto;

                            Cuentas recordOrigen = new Cuentas();
                            recordOrigen.setSaldo(nuevoMontoOrigen);
                            Cuentas recordDestino = new Cuentas();
                            recordDestino.setSaldo(nuevoMontoDestino);

                            //Se actualiza las cuentas
                            cuentasFacade.updateByExampleSelective(recordOrigen, cuentasOrigenExample);
                            cuentasFacade.updateByExampleSelective(recordDestino, cuentasDestinoExample);

                            //Hacemos expirar el token
                            Tokens tokens = new Tokens();
                            tokens.setToken(token);
                            tokens.setEstado(Boolean.FALSE);
                            tokensFacade.updateByExampleSelective(tokens, tokensExample);

                            resp.setEstado(0);
                            resp.setMensaje("OK");

                        } else {
                            resp.setEstado(-1);
                            resp.setMensaje("Cuenta Destino invalida o inactiva");
                        }
                    } else {
                        resp.setEstado(-1);
                        resp.setMensaje("Saldo insuficienta para la transferencia");
                    }
                } else {
                    resp.setEstado(-1);
                    resp.setMensaje("Cuenta Origen no valida o inactiva");
                }
            } else {
                resp.setEstado(-1);
                resp.setMensaje("tokens no valido");
            }

        } catch (Exception e) {
            logger.info(e.getMessage(), e);
            resp.setEstado(-1);
            resp.setMensaje(e.getMessage());
        }
        return resp;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "extraccion")
    public BaseRespuesta extraccion(@WebParam(name = "cuenta") Integer cuenta, @WebParam(name = "monto") Double monto, @WebParam(name = "token") String token) {
        BaseRespuesta resp = new BaseRespuesta();
        try {
            ctx = new InitialContext();
            tokensFacade = (TokensFacade) ctx.lookup("java:global/CoreBanco/TokensFacade");
            cuentasFacade = (CuentasFacade) ctx.lookup("java:global/CoreBanco/CuentasFacade");

            //verificamos token
            TokensExample tokensExample = new TokensExample();
            tokensExample.createCriteria()
                    .andTokenEqualTo(token)
                    .andEstadoEqualTo(Boolean.TRUE);
            int tokenCount = tokensFacade.countByExample(tokensExample);

            if (tokenCount > 0) {
                //verficamos cuenta origen
                CuentasExample cuentasOrigenExample = new CuentasExample();
                cuentasOrigenExample.createCriteria()
                        .andNumeroCuentaEqualTo(cuenta)
                        .andEstadoEqualTo(Boolean.TRUE);
                List<Cuentas> cuentas = cuentasFacade.selectByExample(cuentasOrigenExample);
                if (cuentas.size() > 0) {
                    Cuentas cuentaOrigen = cuentas.get(0);
                    //verificamos que tenga saldo suficiente para la transferencia
                    if (cuentaOrigen.getSaldo() >= monto) {
                        //verificamos cuenta destino
                        CuentasExample cuentasDestinoExample = new CuentasExample();
                        cuentasDestinoExample.createCriteria()
                                .andNumeroCuentaEqualTo(1)
                                .andEstadoEqualTo(Boolean.TRUE);
                        List<Cuentas> cuentasDestinos = cuentasFacade.selectByExample(cuentasDestinoExample);
                        if (cuentasDestinos.size() > 0) {
                            Cuentas destinoCuenta = cuentasDestinos.get(0);
                            Double nuevoMontoOrigen = cuentaOrigen.getSaldo() - monto;
                            Double nuevoMontoDestino = destinoCuenta.getSaldo() + monto;

                            Cuentas recordOrigen = new Cuentas();
                            recordOrigen.setSaldo(nuevoMontoOrigen);
                            Cuentas recordDestino = new Cuentas();
                            recordDestino.setSaldo(nuevoMontoDestino);

                            //Se actualiza las cuentas
                            cuentasFacade.updateByExampleSelective(recordOrigen, cuentasOrigenExample);
                            cuentasFacade.updateByExampleSelective(recordDestino, cuentasDestinoExample);

                            //Hacemos expirar el token
                            Tokens tokens = new Tokens();
                            tokens.setToken(token);
                            tokens.setEstado(Boolean.FALSE);
                            tokensFacade.updateByExampleSelective(tokens, tokensExample);

                            resp.setEstado(0);
                            resp.setMensaje("OK");

                        } else {
                            resp.setEstado(-1);
                            resp.setMensaje("Cuenta Destino invalida o inactiva");
                        }
                    } else {
                        resp.setEstado(-1);
                        resp.setMensaje("Saldo insuficienta para la transferencia");
                    }
                } else {
                    resp.setEstado(-1);
                    resp.setMensaje("Cuenta Origen no valida o inactiva");
                }

            } else {
                resp.setEstado(-1);
                resp.setMensaje("tokens no valido");
            }

        } catch (Exception e) {
            logger.info(e.getMessage(), e);
            resp.setEstado(-1);
            resp.setMensaje(e.getMessage());
        }
        return resp;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "transferencia")
    public BaseRespuesta transferencia(@WebParam(name = "cuentaOrigen") Integer cuentaOrigen, @WebParam(name = "cuentaDestino") Integer cuentaDestino, @WebParam(name = "monto") Double monto, @WebParam(name = "token") String token) {
        logger.info("IN [{}]", cuentaOrigen, cuentaDestino, monto);
        BaseRespuesta resp = new BaseRespuesta();
        try {
            ctx = new InitialContext();
            tokensFacade = (TokensFacade) ctx.lookup("java:global/CoreBanco/TokensFacade");
            cuentasFacade = (CuentasFacade) ctx.lookup("java:global/CoreBanco/CuentasFacade");

            //verificamos token
            TokensExample tokensExample = new TokensExample();
            tokensExample.createCriteria()
                    .andTokenEqualTo(token)
                    .andEstadoEqualTo(Boolean.TRUE);
            int tokenCount = tokensFacade.countByExample(tokensExample);

            if (tokenCount > 0) {
                //verficamos cuenta origen
                CuentasExample cuentasOrigenExample = new CuentasExample();
                cuentasOrigenExample.createCriteria()
                        .andNumeroCuentaEqualTo(cuentaOrigen)
                        .andEstadoEqualTo(Boolean.TRUE);
                List<Cuentas> cuentas = cuentasFacade.selectByExample(cuentasOrigenExample);
                if (cuentas.size() > 0) {
                    Cuentas cuenta = cuentas.get(0);
                    //verificamos que tenga saldo suficiente para la transferencia
                    if (cuenta.getSaldo() >= monto) {
                        //verificamos cuenta destino
                        CuentasExample cuentasDestinoExample = new CuentasExample();
                        cuentasDestinoExample.createCriteria()
                                .andNumeroCuentaEqualTo(cuentaDestino)
                                .andEstadoEqualTo(Boolean.TRUE);
                        List<Cuentas> cuentasDestinos = cuentasFacade.selectByExample(cuentasDestinoExample);
                        if (cuentasDestinos.size() > 0) {
                            Cuentas destinoCuenta = cuentasDestinos.get(0);
                            Double nuevoMontoOrigen = cuenta.getSaldo() - monto;
                            Double nuevoMontoDestino = destinoCuenta.getSaldo() + monto;

                            Cuentas recordOrigen = new Cuentas();
                            recordOrigen.setSaldo(nuevoMontoOrigen);
                            Cuentas recordDestino = new Cuentas();
                            recordDestino.setSaldo(nuevoMontoDestino);

                            //Se actualiza las cuentas
                            cuentasFacade.updateByExampleSelective(recordOrigen, cuentasOrigenExample);
                            cuentasFacade.updateByExampleSelective(recordDestino, cuentasDestinoExample);

                            //Hacemos expirar el token
                            Tokens tokens = new Tokens();
                            tokens.setToken(token);
                            tokens.setEstado(Boolean.FALSE);
                            tokensFacade.updateByExampleSelective(tokens, tokensExample);

                            resp.setEstado(0);
                            resp.setMensaje("OK");

                        } else {
                            resp.setEstado(-1);
                            resp.setMensaje("Cuenta Destino invalida o inactiva");
                        }
                    } else {
                        resp.setEstado(-1);
                        resp.setMensaje("Saldo insuficienta para la transferencia");
                    }
                } else {
                    resp.setEstado(-1);
                    resp.setMensaje("Cuenta Origen no valida o inactiva");
                }

            } else {
                resp.setEstado(-1);
                resp.setMensaje("tokens no valido");
            }

        } catch (Exception e) {
            logger.info(e.getMessage(), e);
            resp.setEstado(-1);
            resp.setMensaje(e.getMessage());
        }
        logger.info("OUT: " + resp);
        return resp;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "transferenciaMoneda")
    public BaseRespuesta transferenciaMoneda(
            @WebParam(name = "cuentaOrigen") Integer cuentaOrigen,
            @WebParam(name = "cuentaDestino") Integer cuentaDestino,
            @WebParam(name = "monto") Double monto,
            @WebParam(name = "tipoMoneda") String tipoMoneda,
            @WebParam(name = "token") String token) {
        logger.info("IN [{}]", cuentaOrigen, cuentaDestino, monto);
        BaseRespuesta resp = new BaseRespuesta();
        Gson gson = new Gson();
        try {
            ctx = new InitialContext();
            usuariosFacade = (UsuariosFacade) ctx.lookup("java:global/CoreBanco/UsuariosFacade");
            cuentasFacade = (CuentasFacade) ctx.lookup("java:global/CoreBanco/CuentasFacade");

            //verificamos token
            TokensExample tokensExample = new TokensExample();
            tokensExample.createCriteria()
                    .andTokenEqualTo(token)
                    .andEstadoEqualTo(Boolean.TRUE);
            int tokenCount = tokensFacade.countByExample(tokensExample);

            if (tokenCount > 0) {
                //verficamos cuenta origen
                CuentasExample cuentasOrigenExample = new CuentasExample();
                cuentasOrigenExample.createCriteria()
                        .andNumeroCuentaEqualTo(cuentaOrigen)
                        .andEstadoEqualTo(Boolean.TRUE);
                List<Cuentas> cuentas = cuentasFacade.selectByExample(cuentasOrigenExample);
                if (cuentas.size() > 0) {
                    Cuentas cuenta = cuentas.get(0);

                    //Realizamos el cambio de moneda
                    py.sd.casacambio.LoginRespuesta loginRespuesta = new py.sd.casacambio.LoginRespuesta();
                    
                    
                    try {
                        loginRespuesta = CasaCambioConsume.login(Constantes.USER, Constantes.PASSWORD);
                    } catch (Exception e) {
                        logger.error(e.getMessage(),e);
                        resp.setEstado(-1);
                        resp.setMensaje("Error indeterminado");
                    }
                    if (loginRespuesta.getEstado() == 0) {
                        logger.info("Login exitoso");
                        logger.info(gson.toJson(loginRespuesta));
                        py.sd.casacambio.CambioRespuesta cambioRespuesta = CasaCambioConsume.compra(Integer.valueOf(tipoMoneda), monto, loginRespuesta.getToken());
                        if (cambioRespuesta.getEstado() == 0) {
                            monto = cambioRespuesta.getMonto();
                        } else {
                            logger.info("servidor de Casa Cambio no disponible");
                            resp.setEstado(-1);
                            resp.setMensaje("Error ");
                            logger.info("OUT: " + gson.toJson(resp));
                            return resp;
                        }

                    } else {
                        logger.info("servidor de Casa Cambio no disponible");
                        resp.setEstado(-1);
                        resp.setMensaje("Error indeterminado");
                        logger.info("OUT: " + gson.toJson(resp));
                        return resp;
                    }

                    //verificamos que tenga saldo suficiente para la transferencia
                    if (cuenta.getSaldo() > monto) {
                        //verificamos cuenta destino
                        CuentasExample cuentasDestinoExample = new CuentasExample();
                        cuentasDestinoExample.createCriteria()
                                .andNumeroCuentaEqualTo(cuentaDestino)
                                .andEstadoEqualTo(Boolean.TRUE);
                        List<Cuentas> cuentasDestinos = cuentasFacade.selectByExample(cuentasDestinoExample);
                        if (cuentasDestinos.size() > 0) {
                            Cuentas destinoCuenta = cuentasDestinos.get(0);
                            Double nuevoMontoOrigen = cuenta.getSaldo() - monto;
                            Double nuevoMontoDestino = destinoCuenta.getSaldo() + monto;

                            Cuentas recordOrigen = new Cuentas();
                            recordOrigen.setSaldo(nuevoMontoOrigen);
                            Cuentas recordDestino = new Cuentas();
                            recordDestino.setSaldo(nuevoMontoDestino);

                            //Se actualiza las cuentas
                            cuentasFacade.updateByExampleSelective(recordOrigen, cuentasOrigenExample);
                            cuentasFacade.updateByExampleSelective(recordDestino, cuentasDestinoExample);

                            //Hacemos expirar el token
                            Tokens tokens = new Tokens();
                            tokens.setToken(token);
                            tokens.setEstado(Boolean.FALSE);
                            tokensFacade.updateByExampleSelective(tokens, tokensExample);

                            resp.setEstado(0);
                            resp.setMensaje("OK");

                        } else {
                            resp.setEstado(-1);
                            resp.setMensaje("Cuenta Destino invalida o inactiva");
                        }
                    } else {
                        resp.setEstado(-1);
                        resp.setMensaje("Saldo insuficienta para la transferencia");
                    }
                } else {
                    resp.setEstado(-1);
                    resp.setMensaje("Cuenta Origen no valida o inactiva");
                }

            } else {
                resp.setEstado(-1);
                resp.setMensaje("tokens no valido");
            }

        } catch (Exception e) {
            logger.info(e.getMessage(), e);
            resp.setEstado(-1);
            resp.setMensaje(e.getMessage());
        }
        logger.info("OUT: " + resp);
        return resp;
    }

}
